# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# from __future__ import absolute_import
import webapp2
import urllib2
import json
import os
import googleapiclient.discovery
from oauth2client.contrib.appengine import OAuth2DecoratorFromClientSecrets
import httplib2
import hashlib
import datetime
# from google.cloud import bigquery
# from google.cloud import bigtable
# import requests

# The project id whose datasets you'd like to list
PROJECT_ID = 'chicago-traffic-polling'
DATASET_ID = 'chicago_traffic'

# Create the method decorator for oauth.
decorator = OAuth2DecoratorFromClientSecrets(
    os.path.join(os.path.dirname(__file__), 'client_secrets.json'),
    scope='https://www.googleapis.com/auth/bigquery')

# Create the bigquery api client
# http = credentials.authorize(httplib2.Http())
service = googleapiclient.discovery.build('bigquery', 'v2')

class MainPage(webapp2.RequestHandler):
    # # oauth_required ensures that the user goes through the OAuth2
    # # authorization flow before reaching this handler.
    # @decorator.oauth_required
    def get(self):
        # This is an httplib2.Http instance that is signed with the user's
        # credentials. This allows you to access the BigQuery API on behalf
        # of the user.
        # http = decorator.http()
        #
        # response = service.datasets().list(projectId=PROJECTID).execute(http)
        #
        # self.response.out.write('<h3>Hello Chicago: BigQuery</h3>')
        # self.response.out.write('<pre>%s</pre>' %
        #                         json.dumps(response, sort_keys=True, indent=4,
        #                                    separators=(',', ': ')))
        # self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Hello, Chicago!')

class BigQueryTestInsert(webapp2.RequestHandler):
    @decorator.oauth_required
    def get(self):
        http = decorator.http()
        body = {"rows":[
                    {"json": {"Name":"Tim","Id":5}},
                    {"json": {"Name":"Angus","Id":3}},
                    {"json": {"Name":"Reginald","Id":2}}
                ]}

        response = service.tabledata().insertAll(
                       projectId=PROJECT_ID,
                       datasetId='my_new_dataset',
                       tableId='test_table',
                       body=body).execute(http)
        self.response.write(str(response))

class PollTrafficAPI(webapp2.RequestHandler):
    def getInt(self, dict, key):
        try:
            return int(dict[key])
        except:
            return None

    def getFloat(self, dict, key):
        try:
            return float(dict[key])
        except:
            return None

    def formatDate(self, string):
        try:
            split = string.split(" ")
            return split[0] + 'T' + split[1]
        except:
            return None

    def getDateTime(self, dict, key):
        try:
            return self.formatDate(dict[key])
        except:
            return None

    def getString(self, dict, key):
        try:
            return dict[key]
        except:
            return None

    def getId(self, d, idField):
        try:
            m = hashlib.md5()
            m.update(str(self.getString(d, idField) + self.getDateTime(d, "_last_updt")))
            return m.hexdigest()
        except:
            return None

    def pollUrl(self, url):
        try:
            result = urllib2.urlopen(url)
            data = json.loads(result.read())
            return data
        except urllib2.URLError:
            return None

    @decorator.oauth_required
    def doGet(self, url, projectId, datasetId, tableId):
        data = self.pollUrl(url)
        print('Data Length: ' + str(len(data)))
        if data is not None:
            if len(data) > 0:
                dataRowsUnfiltered = map(self.createObject, data)
                dataRows = filter(lambda x: x is not None, dataRowsUnfiltered)
                dataBody = {'rows': dataRows}
                http = decorator.http()
                response = service.tabledata().insertAll(
                               projectId=projectId,
                               datasetId=datasetId,
                               tableId=tableId,
                               body=dataBody).execute(http)

                self.response.write(response) # json.dumps(trafficDataBody))
            else:
                self.response.write('No traffic data')
            self.response.set_status(200)
        else:
            self.response.write('Polling Error 500')
            self.response.set_status(500)

class PollTrafficSegmentAPI(PollTrafficAPI):
    def createObject(self, d):
        print('Create Segment')
        id = self.getId(d, "segmentid")
        timeNow = self.formatDate(datetime.datetime.utcnow().isoformat(' '))

        if id is not None:
            obj = {
                "insertId"  : id,
                "json": {
                    "id"            : id,
                    "time_poll_utc" : timeNow,
                    "_direction"    : self.getString(d, "_direction"),
                    "_fromst"       : self.getString(d, "_fromst"),
                    "_last_updt"    : self.getDateTime(d, "_last_updt"),
                    "_length"       : self.getFloat(d, "_length"),
                    "_lif_lat"      : self.getFloat(d, "_lif_lat"),
                    "_lit_lat"      : self.getFloat(d, "_lit_lat"),
                    "_lit_lon"      : self.getFloat(d, "_lit_lon"),
                    "_strheading"   : self.getString(d, "_strheading"),
                    "_tost"         : self.getString(d, "_tost"),
                    "_traffic"      : self.getInt(d, "_traffic"),
                    "segmentid"     : self.getInt(d, "segmentid"),
                    "start_lon"     : self.getFloat(d, "start_lon"),
                    "street"        : self.getString(d, "street"),
                    "comments"      : self.getString(d, "_comments")
                }
            }
            return obj
        return None

    @decorator.oauth_required
    def get(self):
        url = 'https://data.cityofchicago.org/resource/8v9j-bter.json?$limit=2000'
        projectId = 'chicago-traffic-polling'
        datasetId = 'chicago_traffic'
        tableId = 'segments'

        self.doGet(url, projectId, datasetId, tableId)

        # data = self.pollUrl(url)
        # if data is not None:
        #     if len(data) > 0:
        #         projectId = 'chicago-traffic-polling'
        #         tableId = 'segments'
        #         datasetId = 'chicago_traffic'
        #         dataRowsUnfiltered = map(self.createObject, data)
        #         dataRows = filter(lambda x: x is not None, dataRowsUnfiltered)
        #         dataBody = {'rows': dataRows}
        #         http = decorator.http()
        #         response = service.tabledata().insertAll(
        #                        projectId=projectId,
        #                        datasetId=datasetId,
        #                        tableId=tableId,
        #                        body=dataBody).execute(http)
        #
        #         self.response.write(response) # json.dumps(trafficDataBody))
        #     else:
        #         self.response.write('No traffic data')
        #     self.response.set_status(200)
        # else:
        #     self.response.write('Polling Error 500')
        #     self.response.set_status(500)

class PollTrafficRegionAPI(PollTrafficAPI):
    def createObject(self, d):
        print('Create Region')
        id = self.getId(d, "_region_id")
        timeNow = self.formatDate(datetime.datetime.utcnow().isoformat(' '))

        if id is not None:
            obj = {
                "insertId"  : id,
                "json": {
                    "id"            : id,
                    "time_poll_utc" : timeNow,
                    "current_speed" : self.getFloat(d, "current_speed"),
                    "_east"         : self.getFloat(d, "_east"),
                    "_last_updt"    : self.getDateTime(d, "_last_updt"),
                    "_region_id"    : self.getInt(d, "_region_id"),
                    "_north"        : self.getFloat(d, "_north"),
                    "_south"        : self.getFloat(d, "_south"),
                    "region"        : self.getString(d, "region"),
                    "_west"         : self.getFloat(d, "_west"),
                    "_description"  : self.getString(d, "_description")
                }
            }
            return obj
        return None

    def get(self):
        url = 'https://data.cityofchicago.org/resource/t2qc-9pjd.json?$limit=2000'
        projectId = 'chicago-traffic-polling'
        datasetId = 'chicago_traffic'
        tableId = 'regions'

        self.doGet(url, projectId, datasetId, tableId)

        # url = 'https://data.cityofchicago.org/resource/t2qc-9pjd.json?$limit=2000'
        # data = self.pollUrl(url)
        # if data is not None:
        #     self.response.write(data)
        #     self.response.set_status(200)
        # else:
        #     self.response.write('Polling Error 500')
        #     self.response.set_status(500)

app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/poll/segment', PollTrafficSegmentAPI),
    ('/poll/region', PollTrafficRegionAPI),
    ('/bigquery/testinsert', BigQueryTestInsert),
    # Create the endpoint to receive oauth flow callbacks
    (decorator.callback_path, decorator.callback_handler())

], debug=True)
